<?php
return [
    'zfr_cors' => [
        'allowed_origins' => ['http://example.com', 'http://localhost:4200', 'chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop'],
        'allowed_methods' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
        'allowed_headers' => ['Authorization', 'Content-Type', 'Access-Control-Allow-Origin'],
        'max_age' => 120,
        'exposed_headers' => [],
        'allowed_credentials' => false,
    ],
];