<?php
namespace Ask\V1\Rpc\AskRouter;

use Zend\Mvc\Controller\AbstractActionController;
use ZF\ContentNegotiation\ViewModel;

class AskRouterController extends AbstractActionController
{
    public function askRouterAction()
    {
        return new ViewModel([
            'ack' => $this->url()->fromRoute('ask.rpc.ask-router',[],['force_canonical' => true])
        ]);
    }
}
