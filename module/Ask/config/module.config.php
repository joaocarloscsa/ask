<?php
return [
    'controllers' => [
        'factories' => [
            'Ask\\V1\\Rpc\\Ping\\Controller' => \Ask\V1\Rpc\Ping\PingControllerFactory::class,
            'Ask\\V1\\Rpc\\AskRouter\\Controller' => \Ask\V1\Rpc\AskRouter\AskRouterControllerFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'ask.rpc.ping' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/ping',
                    'defaults' => [
                        'controller' => 'Ask\\V1\\Rpc\\Ping\\Controller',
                        'action' => 'ping',
                    ],
                ],
            ],
            'ask.rpc.ask-router' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/askrouter',
                    'defaults' => [
                        'controller' => 'Ask\\V1\\Rpc\\AskRouter\\Controller',
                        'action' => 'askRouter',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'ask.rpc.ping',
            1 => 'ask.rpc.ask-router',
        ],
    ],
    'zf-rpc' => [
        'Ask\\V1\\Rpc\\Ping\\Controller' => [
            'service_name' => 'Ping',
            'http_methods' => [
                0 => 'GET',
            ],
            'route_name' => 'ask.rpc.ping',
        ],
        'Ask\\V1\\Rpc\\AskRouter\\Controller' => [
            'service_name' => 'AskRouter',
            'http_methods' => [
                0 => 'GET',
            ],
            'route_name' => 'ask.rpc.ask-router',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'Ask\\V1\\Rpc\\Ping\\Controller' => 'Json',
            'Ask\\V1\\Rpc\\AskRouter\\Controller' => 'Json',
        ],
        'accept_whitelist' => [
            'Ask\\V1\\Rpc\\Ping\\Controller' => [
                0 => 'application/vnd.ask.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'Ask\\V1\\Rpc\\AskRouter\\Controller' => [
                0 => 'application/vnd.ask.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
        ],
        'content_type_whitelist' => [
            'Ask\\V1\\Rpc\\Ping\\Controller' => [
                0 => 'application/vnd.ask.v1+json',
                1 => 'application/json',
            ],
            'Ask\\V1\\Rpc\\AskRouter\\Controller' => [
                0 => 'application/vnd.ask.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-content-validation' => [
        'Ask\\V1\\Rpc\\Ping\\Controller' => [
            'input_filter' => 'Ask\\V1\\Rpc\\Ping\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'Ask\\V1\\Rpc\\Ping\\Validator' => [],
    ],
];
