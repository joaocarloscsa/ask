<?php
namespace Ask\V1\Rpc\AskRouter;

class AskRouterControllerFactory
{
    public function __invoke($controllers)
    {
        return new AskRouterController();
    }
}
